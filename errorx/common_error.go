package errorx

// 未授权需要登录
func NewUnauthorizedError(msg string) Error {
	return NewError(401, msg)
}

// 无权限
func NewForbiddenError(msg string) Error {
	return NewError(403, msg)
}

// 参数错误
func NewParamError(msg string) Error {
	return NewError(406, msg)
}

// 系统内部错误
func NewInternalServerError(msg string) Error {
	return NewError(500, msg)
}
