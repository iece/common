package oss

import (
	"errors"

	"github.com/spf13/viper"
)

type RrsOssOptions struct {
	Endpoint  string `yaml:"endpoint"`
	AppKey    string `yaml:"appKey"`
	AppSecret string `yaml:"appSecret"`
}

func (rou *RrsOssOptions) Load() error {
	rou.Endpoint = viper.GetString("aliyun.oss.endpoint")
	if rou.Endpoint == "" {
		return errors.New("no endpint setting")
	}

	rou.AppKey = viper.GetString("aliyun.appKey")
	if rou.AppKey == "" {
		return errors.New("oss appkey is emtpty")
	}

	rou.AppSecret = viper.GetString("aliyun.appSecret")
	if rou.AppSecret == "" {
		return errors.New("oss app secret is empty")
	}

	return nil
}
