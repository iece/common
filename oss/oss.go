package oss

import (
	"io"
)

// oss文件目录
const (
	API_SPECIFIC    = "api_specific"   // 用于放置swagger open api定义文件
	BANK_IMAGES     = "bankImages"     // 银行卡照片放置地方
	CONTRACT        = "contract"       // 合约，或者是合同等文件放置的地方
	EXCEL           = "excel"          // 各种导出表格临时存放地方
	IDENTITY_IMAGES = "identityImages" // 身份证，营业执照，护照等这些用于界定用户身份的文件，图片
	WEBCHAT         = "webchat"        // 小程序用的一些素材，icon等
	IMAGES          = "images"         // 其他类型的图片
	DOC             = "doc"            // 其他类型的文件
)

var dirMap = map[string]string{
	API_SPECIFIC:    "用于放置swagger open api定义文件",
	BANK_IMAGES:     "银行卡照片放置地方",
	CONTRACT:        "合约，或者是合同等文件放置的地方",
	EXCEL:           "各种导出表格临时存放地方",
	IDENTITY_IMAGES: "身份证，营业执照，护照等这些用于界定用户身份的文件，图片",
	WEBCHAT:         "小程序用的一些素材，icon等",
	IMAGES:          "其他类型的图片",
	DOC:             "其他类型的文件",
}

// 从网络地址下载后上传
func UploadURL(picURL, dirName string) (string, error) {
	return rou.UploadURL(picURL, dirName)
}

// 上传二进制
func UploadBytes(bs []byte, dirName, fileName string) (string, error) {
	return rou.UploadBytes(bs, dirName, fileName)
}

// 上传流数据
func UploadReader(reader io.Reader, dirName, fileName string) (string, error) {
	return rou.UploadReader(reader, dirName, fileName)
}
