package sms

import (
	"encoding/json"

	"gitee.com/iece/common/log"
)

const (
	ScaleFinishedTemplateCode     = "TODO"          // 过磅完成通知货主短信模版code
	DischargeFinishedTemplateCode = "TODO"          // 卸货完成短信模版code
	CaptchaTemplateCode           = "SMS_461866170" // 登录注册验证码
	Captcha15mTemplateCode        = "SMS_461866170" // 15分钟验证码
	LoadingFinishedTemplateCode   = "TODO"          // 装货完成短信模版code
	JinhuaWarningTemplateCode     = "TODO"          // 金华仓储预警
	TrunkLineUpCallTemplateCode   = "TODO"          // 排队车辆叫号提醒
)

// ScaleFinishedParam 过磅完成短信参数
type ScaleFinishedParam struct {
	Truck       string `json:"truck"`       // 车辆（车牌）
	GrossWeight string `json:"grossWeight"` // 毛重
	TareWeight  string `json:"tareWeight"`  // 皮重
	Impurity    string `json:"impurity"`    // 扣杂
	Deduction   string `json:"deduction"`   // 扣款
}

func (s ScaleFinishedParam) ToJson() string {
	data, err := json.Marshal(s)
	if err != nil {
		log.Error("ScaleFinishedParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}

// DischargeFinishedParam 卸货完成短信参数
type DischargeFinishedParam struct {
	Truck     string `json:"truck"`     // 车辆（车牌）
	Goods     string `json:"goods"`     // 商品
	Impurity  string `json:"impurity"`  // 扣杂
	Deduction string `json:"deduction"` // 扣款
	Weight    string `json:"weight"`    // 扣杂净重
	Amount    string `json:"amount"`    // 金额
}

func (d DischargeFinishedParam) ToJson() string {
	data, err := json.Marshal(d)
	if err != nil {
		log.Error("DischargeFinishedParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}

// CaptchaParam 验证码参数
type CaptchaParam struct {
	Code string `json:"code"`
}

func (c CaptchaParam) ToJson() string {
	data, err := json.Marshal(c)
	if err != nil {
		log.Error("CaptchaParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}

// LoadingFinishedParam 装货完成短信参数
type LoadingFinishedParam struct {
	Truck     string `json:"truck"`     // 车辆（车牌）
	Goods     string `json:"goods"`     // 商品
	Impurity  string `json:"impurity"`  // 扣杂
	Deduction string `json:"deduction"` // 扣款
	Weight    string `json:"weight"`    // 扣杂净重
	Amount    string `json:"amount"`    // 金额
}

func (l LoadingFinishedParam) ToJson() string {
	data, err := json.Marshal(l)
	if err != nil {
		log.Error("LoadingFinishedParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}

type JinhuaWarningParam struct {
	Name    string `json:"name"`    // 名称
	Product string `json:"product"` // 危废信息
	Type    string `json:"type"`    // 提醒类型
	Amount  string `json:"amount"`  // 阀值
}

func (j JinhuaWarningParam) ToJson() string {
	data, err := json.Marshal(j)
	if err != nil {
		log.Error("LoadingFinishedParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}

type TrunkLineUpCallParam struct {
	TruckNum string `json:"truckNum"` // 车号
}

func (j TrunkLineUpCallParam) ToJson() string {
	data, err := json.Marshal(j)
	if err != nil {
		log.Error("TrunkLineUpCallParam, struct convert json err : %+v", err)
		return ""
	}
	return string(data)
}
