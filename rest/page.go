package rest

type PageRequest struct {
	Page     int `json:"page" form:"page"`         // 第几页 默认1
	PageSize int `json:"pageSize" form:"pageSize"` // 页大小 默认10
}

// db.Limit(limit).Offset(offset)
func (req *PageRequest) GetLimitOffset() (limit, offset int) {
	if req.Page < 1 {
		req.Page = 1
	}
	if req.PageSize < 1 {
		req.PageSize = 10
	}
	return req.PageSize, (req.Page - 1) * req.PageSize
}

// 生成分页结果
func (req PageRequest) NewPageResult() PageResult {
	return NewPageResult(req.Page, req.PageSize)
}

type PageResult struct {
	Page       int `json:"page"`       // 第几页
	PageSize   int `json:"pageSize"`   // 页大小
	TotalPages int `json:"totalPages"` // 总页数
	Total      int `json:"total"`      // 总条数
	List       any `json:"list"`       // 列表数据
}

// 生成分页结果
// page 默认1
// pageSize 默认10
func NewPageResult(page, pageSize int) PageResult {
	if page < 1 {
		page = 1
	}
	if pageSize < 1 {
		pageSize = 10
	}
	return PageResult{
		Page:     page,
		PageSize: pageSize,
	}
}

// 设置总数
func (res *PageResult) SetTotal(total int) {
	res.Total = total
	res.TotalPages = total / res.PageSize
	if total%res.PageSize > 0 {
		res.TotalPages++
	}
}

// 设置返回列表
func (res *PageResult) SetList(list any) {
	res.List = list
}
