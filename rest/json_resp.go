package rest

import (
	"gitee.com/iece/common/errorx"
)

type JsonResp struct {
	Success bool   `json:"success"` // 是否成功
	Code    int    `json:"code"`    // 200 成功
	Msg     string `json:"msg"`
	Data    any    `json:"data"`
}

func JsonRespData(data any, err error) JsonResp {
	resp := JsonResp{Data: data}
	if err == nil {
		resp.Success = true
		resp.Code = 200
		return resp
	}

	switch e := err.(type) {
	case errorx.Error:
		resp.Code = e.Code
		resp.Msg = e.Msg
	case *errorx.Error:
		resp.Code = e.Code
		resp.Msg = e.Msg
	default:
		resp.Code = 500
		resp.Msg = "系统错误"
	}

	return resp
}
