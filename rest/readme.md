### 返回值定义

1. http请求返回值统一使用`rest.JsonResp`包裹
2. `code`为200时标识成功，其他失败，失败描述参考[errorCode定义](../errorx/readme.md)
3. `code`小于1000时http相应码与`code`相同http相应码未定义的为500
4. 业务数据放到`data`中

#### 前后端接口约定

1. POST请求参数格式仅接受json格式
2. GET请求参数仅接受query格式
3. GET请求参数过多或参数复杂可使用POST请求参数使用json格式
4. 只使用GET/POST 例如：

```http request
### 获取商品
GET http://127.0.0.1:8080/item/get-item?id=1
Content-Type: application/json

### 获取商品规格
GET http://127.0.0.1:8080/item/get-sku?itemId=1
Content-Type: application/json

### 获取商品价格 (复杂获取请求)
POST http://127.0.0.1:8080/item/get-item-price
Content-Type: application/json

{
  "name":"AAA",
  "orderBy":[{  
    "filed":"item_no",
    "desc":false
  }]
}

### 新建商品
POST http://127.0.0.1:8080/item/creat-item
Content-Type: application/json

{
"name":"AAA",
"itemNo":"aaaa"
}

### 删除商品
POST http://127.0.0.1:8080/item/del-item
Content-Type: application/json

{
"id":1
}

### 修改商品名称
POST http://127.0.0.1:8080/item/update-item-name
Content-Type: application/json

{
"id":1,
"name":"bbbbb",
}

```
