package rest

import (
	"errors"
	"reflect"
	"testing"

	"gitee.com/iece/common/errorx"
)

func TestJsonRespData(t *testing.T) {
	type args struct {
		data any
		err  error
	}
	newError := errorx.NewError(20000, "20000")
	tests := []struct {
		name string
		args args
		want JsonResp
	}{
		{
			name: "err",
			args: args{
				err: newError,
			},
			want: JsonResp{Code: 20000, Msg: "20000"},
		},
		{
			name: "err",
			args: args{
				err: &newError,
			},
			want: JsonResp{Code: 20000, Msg: "20000"},
		},
		{
			name: "err",
			args: args{
				err: errors.New("ddasfew"),
			},
			want: JsonResp{Code: 500, Msg: "系统错误"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JsonRespData(tt.args.data, tt.args.err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("JsonRespData() = %v, want %v", got, tt.want)
			}
		})
	}
}
