package utils

import (
	"math/rand"
	"strconv"
	"sync"

	"gitee.com/iece/common/conf"

	"github.com/yitter/idgenerator-go/idgen"
)

var genIDOnce sync.Once

func GenID() int64 {
	genIDOnce.Do(func() {
		appID := conf.Config.AppId
		if appID == 0 {
			appID = rand.Intn(64)
		}
		ops := idgen.NewIdGeneratorOptions(uint16(appID))
		idgen.SetIdGenerator(ops)
	})
	theID := idgen.NextId()
	return theID
}

func GenIDStr() string {
	theID := GenID()
	return strconv.FormatInt(theID, 10)
}
