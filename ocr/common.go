package ocr

import (
	"io/ioutil"
	"net/http"
	"strings"

	"gitee.com/iece/common/errorx"
	openApi "github.com/alibabacloud-go/darabonba-openapi/client"
	ocrApi "github.com/alibabacloud-go/ocr-api-20210707/client"
	util "github.com/alibabacloud-go/tea-utils/service"
	"github.com/alibabacloud-go/tea/tea"
)

const (
	smsSignName     = "浙再服"
	accessKeyId     = "LTAI5tLHnxqYvgMviLf7H8PY"
	accessKeySecret = "QWG2Kr6ifQSwzGCc0QpP5PSg2E4B9Q"
)

func CreateClient() (_result *ocrApi.Client, _err error) {
	config := &openApi.Config{
		AccessKeyId:     tea.String(accessKeyId),
		AccessKeySecret: tea.String(accessKeySecret),
	}

	config.Endpoint = tea.String("ocr-api.cn-hangzhou.aliyuncs.com")
	_result = &ocrApi.Client{}
	_result, _err = ocrApi.NewClient(config)
	return _result, _err
}

func doClientRequest(req any, ocrType int) (respData string, err error) {
	client, err := CreateClient()
	if err != nil {
		return "", err
	}

	runtime := &util.RuntimeOptions{}
	tryErr := func() (_e error) {
		defer func() {
			if r := tea.Recover(recover()); r != nil {
				_e = r
			}
		}()

		switch ocrType {
		case BusinessOcr:
			respData, _e = doRequestBusinessOcr(client, req, runtime)
		case IdCardFrontOcr:
			respData, _e = doRequestIdCardFrontOcr(client, req, runtime)
		case IdCardBackOcr:
			respData, _e = doRequestIdCardBackOcr(client, req, runtime)
		case BankCardOcr:
			respData, _e = doRequestBankCardOcr(client, req, runtime)
		}
		return _e
	}()

	if tryErr != nil {
		var _e = &tea.SDKError{}
		if _t, ok := tryErr.(*tea.SDKError); ok {
			_e = _t
		} else {
			_e.Message = tea.String(tryErr.Error())
		}
		return "", errorx.NewError(10000, *util.AssertAsString(_e.Message))
	}

	return respData, nil
}

func doRequestBusinessOcr(client *ocrApi.Client, req any, runtime *util.RuntimeOptions) (respData string, err error) {
	resp, err := client.RecognizeBusinessLicenseWithOptions(req.(*ocrApi.RecognizeBusinessLicenseRequest), runtime)
	if err != nil {
		return "", err
	}

	if resp.Body != nil {
		return *resp.Body.Data, nil
	}
	return "", nil
}

func doRequestIdCardFrontOcr(client *ocrApi.Client, req any, runtime *util.RuntimeOptions) (string, error) {
	resp, err := client.RecognizeIdcardWithOptions(req.(*ocrApi.RecognizeIdcardRequest), runtime)
	if err != nil {
		return "", err
	}

	if resp.Body != nil {
		return *resp.Body.Data, nil
	}
	return "", nil
}

func doRequestIdCardBackOcr(client *ocrApi.Client, req any, runtime *util.RuntimeOptions) (string, error) {
	resp, err := client.RecognizeIdcardWithOptions(req.(*ocrApi.RecognizeIdcardRequest), runtime)
	if err != nil {
		return "", err
	}

	if resp.Body != nil {
		return *resp.Body.Data, nil
	}
	return "", nil
}

func doRequestBankCardOcr(client *ocrApi.Client, req any, runtime *util.RuntimeOptions) (string, error) {
	resp, err := client.RecognizeBankCardWithOptions(req.(*ocrApi.RecognizeBankCardRequest), runtime)
	if err != nil {
		return "", err
	}

	if resp.Body != nil {
		return *resp.Body.Data, nil
	}
	return "", nil
}

func copyURLToImage(ossUrl string) ([]byte, error) {
	// oss下载文件
	resp, err := http.Get(ossUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func doHttpRequest(ocrUrl string, body string, contentType string) ([]byte, error) {
	req, err := http.NewRequest("POST", ocrUrl, strings.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Authorization", "APPCODE "+ocrAppCode)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return bodyText, err
}
