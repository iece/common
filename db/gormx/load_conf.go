package gormx

import (
	"gitee.com/iece/common/conf"
	"github.com/spf13/viper"
)

func loadMysqlConfig(mysqlConfKey string) (c MysqlConfig, err error) {
	sub := viper.Sub(mysqlConfKey)
	err = sub.Unmarshal(&c, conf.DecoderConfigTagYaml)
	if err != nil {
		return c, err
	}
	return c, nil
}
